µ-store
===
Microstore is contrived pair of services for order placement and history, realised as microservices, targeting a Kubernetes-based deployment.

# 1. Basic microservice with REST and a DB
A basic microservice that exposes a REST interface, letting clients place, amend and view orders. Authentication is not a requirement.

* Project should be hosted on Gitlab
* Use Spring Boot
* Use Flyway for DB schema definition
* Data should be persisted in a Postgres DB
* Use Gradle to produce an [Uber Jar](https://github.com/johnrengelman/shadow)
* APIs should be documented and published using Swagger
* Use Mockito and JUnit as appropriate
* Use Wiremock as appropriate
* Logging with Log4j 2.11
* Service should be runnable from IDE (IntelliJ) and command-line (CLI)
* Use a single Bash script to launch the app from CLI, e.g. `run.sh`

# 2. Set up a Continuous Integration environment
* Use Gitlab-CI to build your project automatically on each commit, run the unit tests, etc.

# 3. Containers
* Containerise your application, so that it can run be with `docker`
* Ideally, a docker build should be initiated by Gradle (do this after the first point)
* Create a `docker-compose` file so that your application can be started together with Postgres 
* Ideally anyone can run your app on their machine without having the JDK/JRE or Postgres installed

# 4. Publish events and aggregate order history
* Run a Kafka broker instance locally (use the `obsidiandynamics/kafka` for simplicity)
* Enhance the _Order_ service to publish a Kafka event upon the creation and subsequent updates to orders. Updates should be in JSON format and should describe the change to the order.
* Create a new _Order History_ service, that processes _Order_ updates and keeps track of history in its own DB (can just use a different DB schema for simplicity)
* Create a new REST interface for viewing/searching order history

# 5. Performance-testing the service
* Curious to see how fast _Order_ runs; use [JMeter](https://jmeter.apache.org/) to profile a basic mix of REST calls

# 6. Deploying
* Create a Google Cloud Platform (GCP) account
* Create an GKE (Google Kubernetes Engine) cluster on GCP
* Create a Cloud SQL for Postgres on GCP
* Use Google [click-to-deploy image](https://console.cloud.google.com/marketplace/details/click-to-deploy-images/kafka) to deploy a single-node Kafka broker
* Deploy your services to GKE using `kubectl` (note: you might want to add a health check endpoint)
* Automate the deployment using Gitlab-CI

# 7. Centralised logging
* Add a new Log4j2 appender that can output to an ELK stack
* Use a [logz.io](https://logz.io) community account to set up a managed ELK

# 8. Metrics
* Expose the frequency of REST API calls via [Prometheus](https://prometheus.io/) metrics
* Run Prometheus as a pod on GKE
* Write to a free managed [Grafana](https://grafana.com/get) instance

# 99. Bonus tasks
* Gradle build should generate a JaCoCo coverage report
* Use [Codecov.io](https://codecov.io/) to host coverage metrics; submission should occur from the Gitlab-CI runner